
type ApiPrice = {
    data: {
      name: string,
      symbol: string,
      price: string,
      // eslint-disable-next-line camelcase
      price_BNB: string
    },
    // eslint-disable-next-line camelcase
    update_at: string
  }

/**
 * 
 * @returns {"name":"Grabity","symbol":"GBT","price":"0","price_BNB":"0"}
 */
export default async function getGBTPrice() {
    let data: ApiPrice;
    try {
      const response = await fetch('https://api.pancakeswap.info/api/v2/tokens/0x7442c62db4a07615f822a8a43d318eaef9edf0c2');
      const responseJson = await response.json();
      data = responseJson;
     } catch(error) {
      console.error(error);
    }
    return data;
  }
  