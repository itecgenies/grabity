import contracts from './contracts'
import { FarmConfig, QuoteToken } from './types'

const farms: FarmConfig[] = [
  {
    pid: 0,
    risk: 5,
    lpSymbol: 'BUSD-GBT-LP',
    lpAddresses: {
      97: '0x53283414af6d2d2c853c736c1297651f1Fa5f379',
      56: '0x2aE5D91cd991a10F686019843732836bf9757714',
    },
    tokenSymbol: 'GBT',
    tokenAddresses: {
      97: '0xe2b52d9f7b55F191f7A26c7D4B73E309b918eF3a',
      56: '0xD800D37C533B157DC1Cb5B6907931DAAF93B737E',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 1,
    risk: 5,
    lpSymbol: 'BNB-GBT-LP',
    lpAddresses: {
      97: '0x75B7f53b9c879FB1c3b1f8EA7cb3282aF52aD8C2',
      56: '0xa028dea528d115c005681693a41aa9a1eacf621e',
    },
    tokenSymbol: 'GBT',
    tokenAddresses: {
      97: '0xe2b52d9f7b55F191f7A26c7D4B73E309b918eF3a',
      56: '0xD800D37C533B157DC1Cb5B6907931DAAF93B737E',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 2,
    risk: 3,
    isTokenOnly: true,
    lpSymbol: 'GBT-LP',
    lpAddresses: {
      97: '0x193f37625455D0c563D30F00A574aF0c962e983a',
      56: '0x53283414af6d2d2c853c736c1297651f1Fa5f379', // CAKE-BUSD LP
    },
    tokenSymbol: 'GBT',
    tokenAddresses: {
      97: '0xe2b52d9f7b55F191f7A26c7D4B73E309b918eF3a',
      56: '0xD800D37C533B157DC1Cb5B6907931DAAF93B737E',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
]

export default farms
