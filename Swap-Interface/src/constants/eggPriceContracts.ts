const priceContracts: {cakeAddress: string, busdAddress: string, lpAddress:string} = {
    cakeAddress: '0xD800D37C533B157DC1Cb5B6907931DAAF93B737E',
    busdAddress: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    lpAddress: '0x53283414af6d2d2c853c736c1297651f1Fa5f379'
  }
  
  export default priceContracts