const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validatePoolInput(data) {
    let errors = {};
    data.tokenSymbol = !isEmpty(data.tokenSymbol) ? data.tokenSymbol : "";
    data.tokenAddresses = !isEmpty(data.tokenAddresses) ? data.tokenAddresses : "";
    data.quoteTokenSymbol = !isEmpty(data.quoteTokenSymbol) ? data.quoteTokenSymbol : "";
    data.quoteTokenAdresses = !isEmpty(data.quoteTokenAdresses) ? data.quoteTokenAdresses : "";
    data.logoURI = !isEmpty(data.logoURI) ? data.logoURI : "";

    
    //data.file = !isEmpty(data.file) ? data.file : "";
    if (Validator.isEmpty(data.tokenSymbol)) {
        errors.tokenSymbol = "Token Symbol field is required";
    }
    if (Validator.isEmpty(data.tokenAddresses)) {
        errors.tokenAddresses = "Token Address field is required";
    } 
    if (data.tokenAddresses.length != 42) {
        errors.tokenAddresses = "Address field Must have 42 letters";
    }
    if(data.logoURI.size > 6e+8 ){
    errors.logoURI = "Max Upload size is 600KB only";
    }
    // if (myFIle.size > 1000000) // 2 MiB for bytes.
    /// if (Validator.isEmpty(data.file)) {
    //     errors.file = "Image field is required";
    // }
    
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
};