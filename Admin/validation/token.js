const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateTokenInput(data) {
    // console.log("data>>>",data.addresses.length === 42);
    // name: req.body.name,
    // symbol: req.body.symbol,
    // addresses: req.body.addresses,
    // chainId: req.body.chainId,
    // decimals: req.body.decimals,
    // logoURI: ImageName,
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : "";
    data.symbol = !isEmpty(data.symbol) ? data.symbol : "";
    data.addresses = !isEmpty(data.addresses) ? data.addresses : "";
    
    
    // data.image = !isEmpty(data.file) ? data.image : "";
    if (Validator.isEmpty(data.name)) {
        errors.name = "Name field is required";
    }
    if (Validator.isEmpty(data.symbol)) {
        errors.symbol = "Token Symbol field is required";
    } 
    if (Validator.isEmpty(data.addresses)) {
        errors.addresses = "Address field is required";
    }
    if (data.addresses.length != 42) {
        errors.addresses = "Address field Must have 42 letters";
   }
    
    // if (Validator.isEmpty(data.image)) {
    //     errors.file = "Image field is required";
    // }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
};